<?php

use Illuminate\Database\Seeder;
use App\Readnews;
use Carbon\Carbon;

class FactDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fact_read')->delete();

        $json = File::get("database/fact_read.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            Readnews::create(array(
                'id' => $obj->id,
                'user_id' => $obj->user_id,
                'data_id' => $obj->data_id,
                'status' => $obj->status,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));
        }
    }
}
