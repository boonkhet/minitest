<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Userdata;

class user_dataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_mini')->delete();

        $json = File::get("database/user.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            Userdata::create(array(
                'id' => $obj->id,
                'user_name' => $obj->user_name,
                'password' => $obj->password,
                'name' => $obj->name,
                'l_name' => $obj->l_name,
                'phone' => $obj->phone,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));
        }
    }
}
