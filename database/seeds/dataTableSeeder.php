<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Data;

class dataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     //   DB::table('user_data')->delete();

/*
        for($i=0; $i<=100000; $i++):
            DB::table('user_data')
                ->insert([
                    'title_en' => 'Test',
                    'title_th' => 'Test',
                    'detail_th' => 'Test',
                    'detail_en' => 'Test',
                    'start_date' => Carbon::now()->format('Y-m-d H:i:s'),
                    'stop_date' => Carbon::now()->format('Y-m-d H:i:s'),
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
        endfor;
/*
 *
 *
 */

        DB::table('user_data')->delete();

        $json = File::get("database/data.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            Data::create(array(
                'id' => $obj->id,
                'title_en' => $obj->title_en,
                'title_th' => $obj->title_th,
                'detail_th' => $obj->detail_th,
                'detail_en' => $obj->detail_en,
                'start_date' => $obj->start_date,
                'stop_date' => $obj->stop_date,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));
        }
    }
}
