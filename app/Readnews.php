<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Data;

class Readnews extends Model
{
    protected $table = 'fact_read';

    protected $fillable = [
        'id',
        'user_id',
        'data_id',
    ];

    public function read()
    {
        return $this->hasMany(Data::class,'data_id','id');
    }


}
