<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdata extends Model
{
    protected $table = 'user_mini';



    protected $fillable = [
        'id',
        'user_name',
        'name',
        'l_name',
        'phone',
    ];

    public function newsdata()
    {
        return $this->hasMany(Readnews::class,'user_id','id');
    }

}
