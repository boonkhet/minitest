<?php

namespace App\Http\Controllers;

use App\Data;
use App\User;
use App\Userdata;
use Illuminate\Http\Request;
use App\Readnews;
use Log;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = [];
        $news = Data::all();
        foreach ($news as $key => $new) {
            $datas[$key]['id'] = $new->id;
            $datas[$key]['title_en'] = $new->title_en;
            $datas[$key]['title_th'] = $new->title_th;
            $datas[$key]['detail_th'] = $new->detail_th;
            $datas[$key]['detail_en'] = $new->detail_en;
            $datas[$key]['user_read'] = [];
            $read = $new->read;
            foreach ($read as $readKey => $user_data) {
                $userdata = Userdata::find($user_data->user_id);
                $datas[$key]['user_read'][$readKey]['id'] = $userdata->id;
                $datas[$key]['user_read'][$readKey]['name'] = $userdata->name;
            }
        }

        return response()->json([
            'datas' => $datas,
        ]);


       // return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //1. check  user ว่าเคยเข้าดูรึยัง
        $checkview = Readnews::where('user_id',$request->user_id)->where('data_id',$request->data_id)->count();
        if($checkview > 0){

            return response()->json([
                'msg_return' => 'user view',
                'code_return' => 21,
            ]);

        }else{

            $insert_data = Readnews::create($request->all());
            return response()->json([
                'msg_return' => 'user add view',
                'code_return' => 21,
            ]);

        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = [];
        $news = Userdata::find($id);
        $items['id'] = $news->id;
        $items['name'] = $news->name;
        $items['news'] = [];
        $process_news = $news->newsdata;
        if($process_news){
            foreach ($process_news as $key => $process_news) {
                $data_id = $process_news->data_id;
                $data_news = Data::find($data_id);
                $items['news'][$key]['id'] = $data_news->id;
                $items['news'][$key]['title_en'] = $data_news->title_en;
                $items['news'][$key]['title_th'] = $data_news->title_th;
                $items['news'][$key]['detail_th'] = $data_news->detail_th;
                $items['news'][$key]['detail_en'] = $data_news->detail_en;
                $items['news'][$key]['status'] = $process_news->status;

            }
        }

        return response()->json([
            'data' => $items,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
