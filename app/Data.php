<?php

namespace App;

use App\Readnews;
use Illuminate\Database\Eloquent\Model;


class Data extends Model
{
    protected $table = 'user_data';

    protected $fillable = [
        'id',
        'title_en',
        'title_th',
        'detail_th',
        'detail_en',
        'start_date',
        'stop_date',


    ];

    public function read()
    {
        return $this->hasMany(Readnews::class,'data_id','id');
    }

}
